using UnityEngine;
using System.Collections;

public class Flock : MonoBehaviour
{
    internal FlockController controller;

    internal Rigidbody _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (controller)
        {
            Vector3 relativePos = steer() * Time.deltaTime;

            if (relativePos != Vector3.zero)
                _rigidbody.velocity = relativePos;

            //enforce minimun and maximun speeds for the boids
            float speed = _rigidbody.velocity.magnitude;
            if (speed > controller.MaxVelocity)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * controller.MaxVelocity;
            }
            else if (speed < controller.MinVelocity)
            {
                _rigidbody.velocity = _rigidbody.velocity.normalized * controller.MinVelocity;
            }
        }
    }

    private Vector3 steer()
    {
        Vector3 center = controller.FlockCenter - transform.localPosition;  //Cohesion

        Vector3 velocity = controller.FlockVelocity - _rigidbody.velocity;  //Alignment

        Vector3 follow = controller.Target.localPosition - transform.localPosition;     //Follow leader

        Vector3 separation = Vector3.zero;

        foreach (Flock flock in controller.FlockList)
        {
            if (flock != this)
            {
                Vector3 relativatePos = transform.localPosition - flock.transform.localPosition;

                separation += relativatePos / (relativatePos.sqrMagnitude);
            }
        }

        //Randomize
        Vector3 randomize = new Vector3((Random.value * 2) - 1, (Random.value * 2) - 1, (Random.value * 2) - 1);

        randomize.Normalize();

        return (controller.CenterWeight * center +
                controller.VelocityWeight * velocity +
                controller.SeparationWeight * separation +
                controller.FollowWeight * follow +
                controller.RandomizeWeight * randomize);
    }




}