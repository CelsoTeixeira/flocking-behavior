using UnityEngine;
using System.Collections;

public class FlockController : MonoBehaviour
{

    public float MinVelocity = 1;
    public float MaxVelocity = 8;
    public int FlockSize = 20;

    public float CenterWeight = 1;   //How far the boids should stick to the center

    public float VelocityWeight = 1;    //Alignment behavior

    public float SeparationWeight = 1;  //How far each boid should be separated within the flock

    public float FollowWeight = 1;  //How close each boid should follow to the leader

    public float RandomizeWeight = 1;   //Random noise

    public Flock Prefab;
    public Transform Target;

    internal Vector3 FlockCenter;
    internal Vector3 FlockVelocity;

    public ArrayList FlockList = new ArrayList();

    void Start()
    {
        for (int i = 0; i < FlockSize; i++)
        {
            Flock flock = Instantiate(Prefab, transform.position, transform.rotation) as Flock;
            flock.transform.parent = transform;
            flock.controller = this;
            FlockList.Add(flock);
        }
    }

    void Update()
    {
        Vector3 center = Vector3.zero;
        Vector3 velocity = Vector3.zero;

        foreach (Flock flock in FlockList)
        {
            center += flock.transform.localPosition;
            velocity += flock._rigidbody.velocity;
        }

        FlockCenter = center / FlockSize;
        FlockVelocity = velocity / FlockSize;
    }
}