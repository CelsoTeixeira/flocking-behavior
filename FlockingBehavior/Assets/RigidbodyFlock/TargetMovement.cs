using UnityEngine;
using System.Collections;

public class TargetMovement : MonoBehaviour 
{
    //Move target around circle with tangential speed
    public Vector3 Bound;
    public float Speed = 100.0f;

    private Vector3 _initialPosition;
    private Vector3 _nextMovementPosition;

    void Start()
    {
        _initialPosition = transform.position;
        CalculateNextMovementPosition();
    }

    void CalculateNextMovementPosition()
    {
        float posX = Random.Range(Bound.x, _initialPosition.x + Bound.x);
        float posY = Random.Range(Bound.y, _initialPosition.y + Bound.y);
        float posZ = Random.Range(Bound.z, _initialPosition.z + Bound.z);

        _nextMovementPosition = _initialPosition + new Vector3(posX, posY, posZ);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.LookRotation(_nextMovementPosition - transform.position), 1.0f*Time.deltaTime);

        if (Vector3.Distance(_nextMovementPosition, transform.position) <= 10.0f)
            CalculateNextMovementPosition();
    }
}