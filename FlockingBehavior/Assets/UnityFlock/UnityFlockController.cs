using UnityEngine;
using System.Collections;

public class UnityFlockController : MonoBehaviour
{
    public Vector3 Offset;
    public Vector3 Bound;
    public float Speed = 100.0f;

    private Vector3 initialPosition;
    private Vector3 nextMovementPosition;

    void Start()
    {
        initialPosition = transform.position;
        CalculateNextMovementPosition();
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.LookRotation(nextMovementPosition = transform.position), 1.0f*Time.deltaTime);

        if (Vector3.Distance(nextMovementPosition, transform.position) <= 10.0f)
        {
            CalculateNextMovementPosition();
        }
    }

    void CalculateNextMovementPosition()
    {
        float posx = Random.Range(initialPosition.x - Bound.x, initialPosition.x + Bound.x);
        float posy = Random.Range(initialPosition.y - Bound.y, initialPosition.y + Bound.y);
        float posz = Random.Range(initialPosition.z - Bound.z, initialPosition.z + Bound.z);

        nextMovementPosition = initialPosition + new Vector3(posx, posy, posz);
    }
}