using UnityEngine;
using System.Collections;

public class UnityFlock : MonoBehaviour
{
    public float MinSpeed = 20.0f;          //Movement speed
    public float TurnSpeed = 20.0f;         //Rotation speed
    public float RandomFreq = 20.0f;        //how many times we want to update the randomPush based on RandomForce
    public float RandomForce = 20.0f;

    //alignment variables
    public float ToOriginForce = 50.0f;     //How spread our flock will be
    public float ToOriginRange = 100.0f;    //keep the boids in range and maintain a distance with the floack's origin

    public float Gravity = 2.0f;

    //separation variables
    //this are used to maintain a minimun distance between individual boids.
    public float AvoidanceRadius = 50.0f;
    public float AvoidanceForce = 20.0f;

    //cohesion variables
    //this are used to keep a minimun distance with the leader or the origin of the flock.
    public float FollowVelocity = 4.0f;
    public float FollowRadius = 40.0f;

    //these variables control the movement of the bold.
    private Transform origin;               //The parent object to control the whole group
    private Vector3 velocity;
    private Vector3 normalizedVelocity;
    private Vector3 randomPush;
    private Vector3 originPush;
    private Transform[] objects;            //Neighboring information
    private UnityFlock[] otherFlocks;       //Neighboring information
    private Transform transformComponent;


    void Start()
    {
        RandomFreq = 1.0f / RandomFreq;

        origin = transform.parent;      //Assing the parent as origin

        transformComponent = transform;     //Cache transform

        //Temporary components;
        Component[] tempFlocks = null;

        //Get all unity flock components from the parent transform in the group
        if (transform.parent)
        {
            tempFlocks = transform.parent.GetComponentsInChildren<UnityFlock>();
        }

        //Assign and sorte all the floack objects in this group
        objects = new Transform[tempFlocks.Length];
        otherFlocks = new UnityFlock[tempFlocks.Length];

        for (int i = 0; i < tempFlocks.Length; i++)
        {
            objects[i] = tempFlocks[i].transform;
            otherFlocks[i] = (UnityFlock)tempFlocks[i];
        }

        //Null Parent as the flock leader will be UnityFlockController object
        transform.parent = null;

        //Calculate random push depends on the random frequency provided
        StartCoroutine(UpdateRandom());

    }

    IEnumerator UpdateRandom()
    {
        while (true)
        {
            //Gets a Vector3 within a sphere with a radius of the RandomForce value
            randomPush = Random.insideUnitSphere*RandomForce;

            //Debug.Log("Calculating randomPush: " + randomPush, this.gameObject);

            yield return new WaitForSeconds(RandomFreq + Random.Range(-RandomFreq / 2.0f, RandomFreq / 2.0f));
        }
    }

    void Update()
    {
        //Internal variables
        float speed = velocity.magnitude;
        Vector3 avgVelocity = Vector3.zero;
        Vector3 avgPosition = Vector3.zero;
        float count = 0;
        float f = 0.0f;
        float d = 0.0f;
        Vector3 myPosition = transformComponent.position;
        Vector3 forceV;
        Vector3 toAvg;
        Vector3 wantedVel;

        for (int i = 0; i < objects.Length; i++)
        {
            Transform transform = objects[i];
            if (transform != transformComponent)
            {
                Vector3 otherPosition = transform.position;

                // Average position to calculate cohesion
                avgPosition += otherPosition;
                count++;

                //Directional vector from other flock to this flock
                forceV = myPosition - otherPosition;

                //Magnitude of that directional vector(Length)
                d = forceV.magnitude;

                //Add push value if the magnitude is less than follow radius to the leader
                if (d < FollowRadius)
                {
                    //calculate the velocity based on the avoidance distance between flocks 
                    //if the current magnitude is less than the specified avoidance radius
                    if (d < AvoidanceRadius)
                    {
                        f = 1.0f - (d / AvoidanceRadius);

                        if (d > 0)
                            avgVelocity += (forceV / d) * f * AvoidanceForce;
                    }

                    //just keep the current distance with the leader
                    f = d / FollowRadius;
                    UnityFlock tempOtherFlock = otherFlocks[i];
                    avgVelocity += tempOtherFlock.normalizedVelocity * f * FollowVelocity;
                }
            }
        }

        if (count > 0)
        {
            //Calculate the average flock velocity(Alignment)
            avgVelocity /= count;

            //Calculate Center value of the flock(Cohesion)
            toAvg = (avgPosition / count) - myPosition;
        }
        else
        {
            toAvg = Vector3.zero;
        }

        //Directional Vector to the leader
        forceV = origin.position - myPosition;
        d = forceV.magnitude;
        f = d / ToOriginRange;

        //Calculate the velocity of the flock to the leader
        if (d > 0)
            originPush = (forceV / d) * f * ToOriginForce;

        if (speed < MinSpeed && speed > 0)
        {
            velocity = (velocity / speed) * MinSpeed;
        }

        wantedVel = velocity;

        //Calculate final velocity
        wantedVel -= wantedVel * Time.deltaTime;
        wantedVel += randomPush * Time.deltaTime;
        wantedVel += originPush * Time.deltaTime;
        wantedVel += avgVelocity * Time.deltaTime;
        wantedVel += toAvg.normalized * Gravity * Time.deltaTime;

        //Final Velocity to rotate the flock into
        velocity = Vector3.RotateTowards(velocity, wantedVel, TurnSpeed * Time.deltaTime, 100.00f);
        transformComponent.rotation = Quaternion.LookRotation(velocity);

        //Move the flock based on the calculated velocity
        transformComponent.Translate(velocity * Time.deltaTime, Space.World);

        //normalise the velocity
        normalizedVelocity = velocity.normalized;
    }
}