This is a simple study about Flocking Behavior.

I'm trying to understand better Flocking, using the book Unity AI Game Programming, Second Edition.

The first algorithm is a simple Flocking Behavior from a previous verson of Unity, he have a cohesion and a separation control over for the boid. It's pretty simple and really easy to read. Also, it's not really heavy on the processing side, I got like 200 entitys on a old machine without huge frame drops.